const DEFENSE = 0
const ATTACK = 1
const ENERGY = 2
let stateFile = require("./state.json")
let helpers = require("./helpers.js")
let flatMap = helpers.flatMap
let unique = helpers.unique
let myself = stateFile.players.find(p => p.playerType === "A")
let opponent = stateFile.players.find(p => p.playerType === "B")
let mapSize = {
  x: stateFile.gameDetails.mapWidth,
  y: stateFile.gameDetails.mapHeight
}
let buildingStats = []
buildingStats[0] = stateFile.gameDetails.buildingsStats.DEFENSE
buildingStats[1] = stateFile.gameDetails.buildingsStats.ATTACK
buildingStats[2] = stateFile.gameDetails.buildingsStats.ENERGY
let gameMap = stateFile.gameMap
let cells = flatMap(gameMap) // all cells on the entire ma
let buildings = flatMap(cells.filter(cell => cell.buildings.length > 0).map(cell => cell.buildings))
let myBuildings = buildings.filter(building => building.playerType == "A")
let myEnergyBuilings = myBuildings.filter(building => building.buildingType == "ENERGY")
let myTurrets = myBuildings.filter(building => building.buildingType == "ATTACK")
let enemyBuildings = buildings.filter(building => building.playerType == "B")
let emptyCells = cells.filter(cell => cell.buildings.length == 0 && cell.x <= mapSize.x / 2 - 1) // cells without buildings on them and on my half of the map
let enemyEnergyBuildings = enemyBuildings.filter(building => building.buildingType == "ENERGY")
let enemyWalls = enemyBuildings.filter(building => building.buildingType == "DEFENSE")
let enemyTurrets = enemyBuildings.filter(building => building.buildingType == "ATTACK")
let missiles = flatMap(cells.filter(cell => cell.missiles.length > 0).map(cell => cell.missiles))
let myEnergyProduction = myEnergyBuilings.length * buildingStats[ENERGY].energyGeneratedPerTurn
let validYCoords = getValidYCoords()
let validXCoords = getValidXCoords()
let defenceCapabilityRating = {
  ENERGY: 1,
  ATTACK: 2,
  DEFENCE: 5
}
let targetPriorityRating = {
  ENERGY: 1,
  ATTACK: 2,
  DEFENCE: 0
}

markEmptyCellsUnderAttackCount()
addEnemyBuildingsAdditionalStats()
console.log(enemyBuildings)
console.log(enemyBuildings)

function initBot(args) {
  key = args[0]
  workingDirectory = args[1]
}

function getValidYCoords() {
  let maxYCoord = mapSize.y - 1
  let validYCoords = []
  for (let index = 0; index <= maxYCoord; index++) {
    validYCoords.push(index)
  }
  return validYCoords
}

function getValidXCoords() {
  let maxXCoord = mapSize.x / 2 - 1
  let validXCoords = []
  for (let index = 0; index <= maxXCoord; index++) {
    validXCoords.push(index)
  }
  return validXCoords
}

function getYCoordsBeingAttacked() {
  return enemyTurrets.reduce((obj, turret) => {
    if (!obj[turret.y]) {
      obj[turret.y] = 0
    }
    obj[turret.y]++
    return obj
  }, {})
}

function markEmptyCellsUnderAttackCount() {
  let yCoordsBeingAttacked = getYCoordsBeingAttacked()
  emptyCells.forEach(emptyCell => {
    emptyCell.opposingTurrets = yCoordsBeingAttacked[emptyCell.y] || 0
  })
}

function addEnemyBuildingsAdditionalStats() {
  let groupedByYCoord = enemyBuildings.reduce((obj, building) => {
    if (!obj[building.y]) {
      obj[building.y] = []
    }
    obj[building.y].push(building)
    return obj
  }, {})
  groupedByYCoord = Object.values(groupedByYCoord)
  groupedByYCoord.forEach(y_group => {
    y_group.forEach(building => {
      console.log(enemyBuildings)
      addDefenseRating(y_group, building)
      addTargetPriority(y_group, building)
    })
  })
}

function addDefenseRating(y_group, building) {
  if (!building.hasOwnProperty("defenseRating")) {
    building.defenceRating = 0
  }
  let buildingsInFront = y_group.filter(buildingInFront => buildingInFront.x < building.x)
  buildingsInFront.forEach(buildingInFront => {
    building.defenceRating = defenceCapabilityRating[buildingInFront.buildingType]
  })
}

function addTargetPriority(y_group, building) {
  // base + buildingPriority + coverProvided - currentTurretsAlreadyAttacking
  if (!building.hasOwnProperty("targetPriority")) {
    building.targetPriority = 0
  }
  console.log(enemyBuildings)

  addPriorityBasedOnBuildingType(building)
  addPriorityBasedOnEnergyCoverProvided(building)
  addPriorityBasedOnAdditionalTargetsBehindInitialTarget(building)
  subtractPriorityBasedOnCurrentTurretsAlreadyAttacking(building)
}

function addPriorityBasedOnBuildingType(building) {
  building.targetPriority += targetPriorityRating[building.buildingType]
}

function addPriorityBasedOnEnergyCoverProvided(building) {
  let energyBuildingsCoverProvided = getEnergyBuildinsgCoverProvidedTo(building.y)
  building.targetPriority += energyBuildingsCoverProvided
}

function addPriorityBasedOnAdditionalTargetsBehindInitialTarget(building) {
  let additionalTargets = getAdditionalBuildingsBehindInitialTarget(building.x, building.y)
  additionalTargets.forEach(additionalTarget => addPriorityBasedOnBuildingType(building))
}

function subtractPriorityBasedOnCurrentTurretsAlreadyAttacking(building) {
  let attackingTurretsCount = getCurrentAttackingTurretsCount(building.y)
  if (attackingTurretsCount && building.buildingType == "ATTACK") {
    building.targetPriority -= 5 // don't attack if already attacking
  } else {
    building.targetPriority -= attackingTurretsCount
  }
}

function getCurrentAttackingTurretsCount(yCoord) {
  return myTurrets.filter(turret => turret.y == yCoord).length
}

function getEnergyBuildinsgCoverProvidedTo(yCoord) {
  return myEnergyBuilings.filter(energy => energy.y == yCoord).length
}

function getAdditionalBuildingsBehindInitialTarget(xCoord, yCoord) {
  return enemyBuildings.filter(building => building.y == yCoord && building.x != xCoord)
}

module.exports = {
  ATTACK,
  DEFENSE,
  ENERGY,
  myself,
  opponent,
  mapSize,
  buildingStats,
  cells,
  buildings,
  myBuildings,
  myEnergyBuilings,
  enemyBuildings,
  missiles,
  emptyCells,
  enemyEnergyBuildings,
  enemyWalls,
  enemyTurrets,
  myEnergyProduction,
  validXCoords,
  validYCoords
}
