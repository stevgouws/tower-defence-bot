// 1.3 //
- Check for lowest opposingTurret count and place furthest back instead of random yCoord for Energy
- think this versions targets ENERGY instead of Attack primarily

// 1.4 //
- markEnemyBuildingsDefenseCount()
- get enemyEnergy targets then get frontmost emptyCell to attack from
- add priorityTarget data & target based on that + defense rating

// 1.5 //
- Change opposingTurrets to vulnerability & add walls to vulnerability rating as posing bigger threat
- Add constructionTimeLeft to priorityRating
- Implement coverProvided rating on emptyCells (how many friendlies cover will be provided to by building here)
- addWallViability
- addEnergyViability
- fix bug where it skips turn at beginning if energy is equal to required energy (also fixed it in 1.3 and 1.4)

// 1.6 //
- Apply vulnerability based on missile impactNextRound
- Check for turrets that will complete next round and add impactNextRound

// 1.7 //
- Implement turretViability based on complicated calculation

// 1.7_b //
- Instead of using turretViability just got all highest priority targets (as in 1.6) then work out most viable turret location
based on lowest "impactInXRounds"