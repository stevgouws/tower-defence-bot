// "use strict"
let fs = require("fs")
let state = require("./state.js")
let helpers = require("./helpers")
let currentRound = state.currentRound
let unique = helpers.unique
let random = helpers.random
let allHighest = helpers.allHighest
let allLowest = helpers.allLowest
let least = helpers.least
let most = helpers.most
let commandFileName = "command.txt"
let myself = state.myself
let opponent = state.opponent
let mapSize = state.mapSize
let buildingStats = state.buildingStats
let cells = state.cells
let buildings = state.buildings
let myBuildings = state.myBuildings
let myEnergyBuildings = state.myEnergyBuilings
let enemyBuildings = state.enemyBuildings
let emptyCells = state.emptyCells
let enemyEnergyBuildings = state.enemyEnergyBuildings
let enemyWalls = state.enemyWalls
let enemyTurrets = state.enemyTurrets
let myEnergyProduction = state.myEnergyProduction
let validXCoords = state.validXCoords
let validYCoords = state.validYCoords
const DEFENSE = state.DEFENSE
const ATTACK = state.ATTACK
const ENERGY = state.ENERGY
run()

function run() {
  if (!haveOptimalEnergy()) {
    return buildEnergy()
  }
  attack()
}

function haveOptimalEnergy() {
  return canAffordTurret() && stillAbleToAffordToBuildEnergyNextRound()
}

function stillAbleToAffordToBuildEnergyNextRound() {
  // basically avoid not being able to build something each round
  return myself.energy + myEnergyProduction - buildingStats[ATTACK].price > buildingStats[ENERGY].price
}

function canAffordTurret() {
  return myself.energy >= buildingStats[ATTACK].price
}

function buildEnergy() {
  let allMostViable = allHighest(emptyCells, "energyViability")
  allMostViable = checkForImminentAttack(allMostViable, ENERGY)
  let { x, y } = most(allMostViable, "energyViability")
  build(x, y, ENERGY)
}

function attack() {
  let allMostViable = allHighest(emptyCells, "turretViability")
  allMostViableNotAboutToBeHit = checkForImminentAttack(allMostViable, ATTACK)
  let { x, y } = most(allMostViableNotAboutToBeHit, "x")
  build(x, y, ATTACK)
}

function checkForImminentAttack(allMostViable, buildingType) {
  let allMostViableNotAboutToBeHit = allMostViable.filter(
    emptyCell => buildingStats[buildingType].constructionTime > emptyCell.impactInXRounds
  )
  if (!allMostViableNotAboutToBeHit.length) {
    return allMostViable
  }
  return allMostViableNotAboutToBeHit
}

function build(x, y, buildingType) {
  if (!passedSafetyChecks(x, y, buildingType)) {
    return doNothing()
  }
  console.log(`${x}, ${y}, ${buildingType}`)
  writeToFile(commandFileName, `${x},${y},${buildingType}`)
}

function passedSafetyChecks(x, y, buildingType) {
  if (!enoughEnergy(buildingType)) {
    throw new Error("Not enough energy to build anything")
    return false
  }
  if (!cellIsEmpty(x, y)) {
    throw new Error("Trying to write to occupied cell")
    return false
  }
  return true
}

function cellIsEmpty(x, y) {
  return Boolean(emptyCells.find(cell => cell.x == x && cell.y == y))
}

function enoughEnergy(buildingType) {
  return buildingStats[buildingType].price <= myself.energy
}

function doNothing() {
  writeToFile(commandFileName, ``)
}

function writeToFile(fileName, payload) {
  fs.writeFile("./" + fileName, payload, err => {
    if (err) {
      return console.log(err)
    }
  })
}
