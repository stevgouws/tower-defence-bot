const DEFENSE = 0
const ATTACK = 1
const ENERGY = 2
let stateFile = require("./state.json")
let helpers = require("./helpers.js")
let flatMap = helpers.flatMap
let unique = helpers.unique
let myself = stateFile.players.find(p => p.playerType === "A")
let opponent = stateFile.players.find(p => p.playerType === "B")
let mapSize = {
  x: stateFile.gameDetails.mapWidth,
  y: stateFile.gameDetails.mapHeight
}
let buildingStats = Object.values(stateFile.gameDetails.buildingsStats)
let gameMap = stateFile.gameMap
let cells = flatMap(gameMap) // all cells on the entire ma
let buildings = flatMap(cells.filter(cell => cell.buildings.length > 0).map(cell => cell.buildings))
let myBuildings = buildings.filter(building => building.playerType == "A")
let myEnergyBuilings = myBuildings.filter(building => building.buildingType == "ENERGY")
let enemyBuildings = buildings.filter(building => building.playerType == "B")
let emptyCells = cells.filter(cell => cell.buildings.length == 0 && cell.x <= mapSize.x / 2 - 1) // cells without buildings on them and on my half of the map
let enemyEnergyBuildings = enemyBuildings.filter(building => building.buildingType == "ENERGY")
let enemyWalls = enemyBuildings.filter(building => building.buildingType == "DEFENSE")
let enemyTurrets = enemyBuildings.filter(building => building.buildingType == "ATTACK")
let missiles = flatMap(cells.filter(cell => cell.missiles.length > 0).map(cell => cell.missiles))
let myEnergyProduction = myEnergyBuilings.length * buildingStats[ENERGY].energyGeneratedPerTurn
let validYCoords = getValidYCoords()
let validXCoords = getValidXCoords()

markEmptyCellsUnderAttackCount()

function initBot(args) {
  key = args[0]
  workingDirectory = args[1]
}

function getValidYCoords() {
  let maxYCoord = mapSize.y - 1
  let validYCoords = []
  for (let index = 0; index <= maxYCoord; index++) {
    validYCoords.push(index)
  }
  return validYCoords
}

function getValidXCoords() {
  let maxXCoord = mapSize.x / 2 - 1
  let validXCoords = []
  for (let index = 0; index <= maxXCoord; index++) {
    validXCoords.push(index)
  }
  return validXCoords
}

function markEmptyCellsUnderAttackCount() {
  let yCoordsBeingAttacked = enemyTurrets.reduce((obj, turret) => {
    if (!obj[turret.y]) {
      obj[turret.y] = 0
    }
    obj[turret.y]++
    return obj
  }, {})

  emptyCells.forEach(emptyCell => {
    emptyCell.opposingTurrets = yCoordsBeingAttacked[emptyCell.y] || 0
  })
}

module.exports = {
  ATTACK,
  DEFENSE,
  ENERGY,
  myself,
  opponent,
  mapSize,
  buildingStats,
  cells,
  buildings,
  myBuildings,
  myEnergyBuilings,
  enemyBuildings,
  missiles,
  emptyCells,
  enemyEnergyBuildings,
  enemyWalls,
  enemyTurrets,
  myEnergyProduction,
  validXCoords,
  validYCoords
}
