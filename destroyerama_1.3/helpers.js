/***
 * Returns an array with one less level of nesting
 * @param array
 * @returns {Array}
 */
function flatMap(array) {
  return array.reduce((acc, x) => acc.concat(x), [])
}

function unique(array) {
  return [...new Set(array)]
}

function random(array) {
  return array[Math.floor(Math.random() * array.length)]
}

function least(obj, attribute) {
  return obj.reduce(function(previous, current) {
    return previous[attribute] < current[attribute] ? previous : current
  })
}

module.exports = {
  flatMap,
  unique,
  random,
  least
}
