// "use strict"
let fs = require("fs")
let { unique, random, allHighest, allLowest, least, most } = require("./helpers")
let commandFileName = "command.txt"
let {
  myself,
  opponent,
  mapSize,
  buildingStats,
  cells,
  buildings,
  myBuildings,
  emptyCells,
  myEnergyProduction,
  DEFENSE,
  ATTACK,
  ENERGY,
  myTurrets,
  enemyTurrets,
  currentRound
} = require("./state.js")
let killSwitchEngaged = readKillSwitchStatus()
run()

function debug() {
  console.log({ killSwitchEngaged })
  console.log({ myTurrets: myTurrets.length })
  console.log({ enemyTurrets: enemyTurrets.length })
}

function readKillSwitchStatus() {
  let data = fs.readFileSync("./killSwitchEngaged.txt", "utf8")
  console.log({ data: data })
  return Boolean(Number(data))
}

function engageKillSwitch() {
  console.log("KILLSWITCH ENGAGED")
  fs.writeFileSync("./killSwitchEngaged.txt", 1)
}

function run() {
  if (!canAffordTurret() || !canStillAffordEnergyTheFollowingRound()) {
    return buildEnergy()
  }
  if (!killSwitchEngaged && losing()) {
    engageKillSwitch()
  }
  if (!killSwitchEngaged) {
    return attack()
  }
  goForKill()
}

function canStillAffordEnergyTheFollowingRound() {
  // basically avoid not being able to build something each round
  return myself.energy + myEnergyProduction - buildingStats[ATTACK].price > buildingStats[ENERGY].price
}

function canAffordTurret() {
  return myself.energy >= buildingStats[ATTACK].price
}

function losing() {
  // SG_TODO: look at again
  let gameIsTight = opponent.health / myself.health < 0.66
  if (!gameIsTight) {
    return false
  }
  return myTurrets.length + 2 <= enemyTurrets.length
}

function buildEnergy() {
  let allMostViable = allHighest(emptyCells, "energyViability")
  allMostViableNotAboutToBeHit = checkForImminentAttack(allMostViable, ENERGY)
  let { x, y } = least(allMostViableNotAboutToBeHit, "x")
  build(x, y, ENERGY)
}

function attack() {
  let allMostViable = allHighest(emptyCells, "turretViability")
  allMostViableNotAboutToBeHit = checkForImminentAttack(allMostViable, ATTACK)
  let { x, y } = most(allMostViableNotAboutToBeHit, "x")
  build(x, y, ATTACK)
}

function goForKill() {
  let allMostViable = allLowest(emptyCells, "enemyRowDefenseRating")
  allMostViableNotAboutToBeHit = checkForImminentAttack(allMostViable, ATTACK)
  let allOnStrongestAttackRow = allHighest(allMostViableNotAboutToBeHit, "rowAttackingTurretsCount")
  let { x, y } = most(allOnStrongestAttackRow, "x")
  build(x, y, ATTACK)
}

function checkForImminentAttack(allMostViable, buildingType) {
  let allMostViableNotAboutToBeHit = allMostViable.filter(
    emptyCell => buildingStats[buildingType].constructionTime > emptyCell.impactInXRounds
  )
  if (!allMostViableNotAboutToBeHit.length) {
    return allMostViable
  }
  return allMostViableNotAboutToBeHit
}

function build(x, y, buildingType) {
  if (!passedSafetyChecks(x, y, buildingType)) {
    return doNothing()
  }
  writeToFile(commandFileName, `${x},${y},${buildingType}`)
}

function passedSafetyChecks(x, y, buildingType) {
  if (!enoughEnergy(buildingType)) {
    console.log("!!!Not enough energy to build anything")
    return false
  }
  if (!cellIsEmpty(x, y)) {
    console.log("!!!Trying to write to occupied cell")
    return false
  }
  return true
}

function cellIsEmpty(x, y) {
  return Boolean(emptyCells.find(cell => cell.x == x && cell.y == y))
}

function enoughEnergy(buildingType) {
  return buildingStats[buildingType].price <= myself.energy
}

function doNothing() {
  writeToFile(commandFileName, ``)
}

function writeToFile(fileName, payload) {
  fs.writeFile("./" + fileName, payload, err => {
    if (err) {
      return console.log(err)
    }
  })
}
