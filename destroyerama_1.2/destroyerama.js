// "use strict"
let fs = require("fs")
let state = require("./state.js")
let commandFileName = "command.txt"
let myself = state.myself
let opponent = state.opponent
let mapSize = state.mapSize
let buildingStats = state.buildingStats
let cells = state.cells
let buildings = state.buildings
let myBuildings = state.myBuildings
let myEnergyBuildings = state.myEnergyBuilings
let enemyBuildings = state.enemyBuildings
let missiles = state.missiles
let emptyCells = state.emptyCells
let enemyEnergyBuildings = state.enemyEnergyBuildings
let enemyWalls = state.enemyWalls
let enemyTurrets = state.enemyTurrets
let myEnergyProduction = state.myEnergyProduction
const DEFENSE = state.DEFENSE
const ATTACK = state.ATTACK
const ENERGY = state.ENERGY
runStrategy()

function runStrategy() {
  if (!haveOptimalEnergy()) {
    buildEnergyOnBackRow()
  } else if (enemyEnergyBuildings.length > 0) {
    attackEnergy()
  } else {
    buildRandomAttack()
  }
  // if (isUnderAttack()) {
  //   defendRow()
  // } else if (hasEnoughEnergyForMostExpensiveBuilding()) {
  //   buildRandom()
  // } else {
  //   doNothing()
  // }
}

function isUnderAttack() {
  // is there a row under attack? and have enough energy to build defence?
  let myDefenders = buildings.filter(b => b.playerType == "A" && b.buildingType == "DEFENSE")
  let opponentAttackers = buildings
    .filter(b => b.playerType == "B" && b.buildingType == "ATTACK")
    .filter(b => !myDefenders.some(d => d.y == b.y))

  return opponentAttackers.length > 0 && myself.energy >= buildingStats[DEFENSE].price
}

function defendRow() {
  // is there a row under attack? and have enough energy to build defence?
  let myDefenders = buildings.filter(b => b.playerType == "A" && b.buildingType == "DEFENSE")
  let opponentAttackers = buildings
    .filter(b => b.playerType == "B" && b.buildingType == "ATTACK")
    .filter(b => !myDefenders.some(d => d.y == b.y))
  if (opponentAttackers.length == 0) {
    buildRandom()
    return
  }
  // choose the first row with an opponent attacker
  let rowNumber = opponentAttackers[0].y
  // get all the x-coordinates for this row, that are empty
  let emptyRowCells = cells.filter(c => c.buildings.length == 0 && c.x <= mapSize.x / 2 - 1 && c.y == rowNumber)
  if (emptyRowCells.length == 0) {
    // cannot build there, try to build somewhere else
    buildRandom()
    return
  }

  let command = { x: "", y: "", bt: "" }
  command.x = getRandomFromArray(emptyRowCells).x
  command.y = rowNumber
  command.bt = 0 // defence building
  buildCommand(command.x, command.y, command.bt)
}

// function hasEnoughEnergyForMostExpensiveBuilding() {
//   return myself.energy >= Math.max(...buildingStats.map(stat => stat.price)) // changed buildingSTats
// }

function haveOptimalEnergy() {
  return myEnergyProduction >= 30 || myself.energy + myEnergyProduction >= 40
}

function getfrontMostEmptyRow() {
  let xValues = emptyCells.map(cell => cell.x)
  let max = Math.max(...xValues)
  console.log("max value is " + max)
  return Math.max(max)
}

function getBackMostEmptyRow() {
  let xValues = emptyCells.map(cell => cell.x)
  return Math.min(...xValues)
}

function buildEnergyOnBackRow() {
  let backMostEmptyRow = getBackMostEmptyRow()
  let randomCell = getRandomFromArray(emptyCells)
  buildCommand(backMostEmptyRow, randomCell.y, ENERGY)
}

function attackEnergy() {
  if (myself.energy < 30) {
    console.log("not enough energy to build ATTACK")
    doNothing()
    return
  }
  let yCoord = enemyEnergyBuildings[0].y
  let xCoord = getfrontMostEmptyRow()
  // let frontMostEmptyRow = getfrontMostEmptyRow()
  // let randomCell = getRandomFromArray(emptyCells)
  buildCommand(xCoord, yCoord, ATTACK)
}

function buildRandomAttack() {
  if (emptyCells.length == 0 || myself.energy < 30) {
    doNothing()
    return
  }
  let randomCell = getRandomFromArray(emptyCells)

  let command = { x: "", y: "", bt: "" }
  command.x = randomCell.x
  command.y = randomCell.y
  buildCommand(command.x, command.y, ATTACK)
}

function buildRandom() {
  if (emptyCells.length == 0) {
    doNothing()
    return
  }
  let randomCell = getRandomFromArray(emptyCells)

  let command = { x: "", y: "", bt: "" }
  command.x = randomCell.x
  command.y = randomCell.y
  command.bt = getRandomInteger(2)
  buildCommand(command.x, command.y, command.bt)
}

function buildCommand(x, y, buildingType) {
  if (!enoughEnergy(buildingType)) {
    doNothing()
    return
  }
  if (cellIsEmpty(x, y)) {
    // SG_TODO: fix reading of empty cells
    writeToFile(commandFileName, `${x},${y},${buildingType}`)
  } else {
    if (y == 7) {
      x--
      y = -1
    }
    buildCommand(x, y + 1, buildingType)
  }
}

function cellIsEmpty(x, y) {
  return Boolean(emptyCells.find(cell => cell.x == x && cell.y == y))
}

function enoughEnergy(buildingType) {
  return buildingStats[buildingType].price < myself.energy
}

function doNothing() {
  writeToFile(commandFileName, ``)
}

function writeToFile(fileName, payload) {
  fs.writeFile("./" + fileName, payload, function(err) {
    console.log("wrote")
    if (err) {
      return console.log(err)
    }
    // console.log(payload);
  })
}

/***
 * Returns a random integer between 0(inclusive) and max(inclusive)
 * @param max
 * @returns {number}
 */
function getRandomInteger(max) {
  return Math.round(Math.random() * max)
}

/**
 * Returns an array that is filled with integers from 0(inclusive) to count(inclusive)
 * @param count
 * @returns {number[]}
 */
function getArrayRange(count) {
  return Array.from({ length: count }, (v, i) => i)
}

/**
 * Return a random element from a given array
 * @param array
 * @returns {*}
 */
function getRandomFromArray(array) {
  return array[Math.floor(Math.random() * array.length)]
}
