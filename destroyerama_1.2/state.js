const DEFENSE = 0
const ATTACK = 1
const ENERGY = 2
let stateFile = require("./state.json")
let myself = stateFile.players.find(p => p.playerType === "A")
let opponent = stateFile.players.find(p => p.playerType === "B")
let mapSize = {
  x: stateFile.gameDetails.mapWidth,
  y: stateFile.gameDetails.mapHeight
}
let buildingStats = Object.values(stateFile.gameDetails.buildingsStats)
let gameMap = stateFile.gameMap
let cells = flatMap(gameMap) // all cells on the entire ma
let buildings = flatMap(cells.filter(cell => cell.buildings.length > 0).map(cell => cell.buildings))
let myBuildings = buildings.filter(building => building.playerType == "A")
let myEnergyBuilings = myBuildings.filter(building => building.buildingType == "ENERGY")
let enemyBuildings = buildings.filter(building => building.playerType == "B")
let emptyCells = cells.filter(cells => cells.buildings.length == 0 && cells.x <= mapSize.x / 2 - 1) // cells without buildings on them and on my half of the map
let enemyEnergyBuildings = enemyBuildings.filter(building => building.buildingType == "ENERGY")
let enemyWalls = enemyBuildings.filter(building => building.buildingType == "DEFENSE")
let enemyTurrets = enemyBuildings.filter(building => building.buildingType == "ATTACK")
let missiles = flatMap(cells.filter(cell => cell.missiles.length > 0).map(cell => cell.missiles))
let myEnergyProduction = myEnergyBuilings.length * buildingStats[ENERGY].energyGeneratedPerTurn
module.exports = {
  ATTACK,
  DEFENSE,
  ENERGY,
  myself,
  opponent,
  mapSize,
  buildingStats,
  cells,
  buildings,
  myBuildings,
  myEnergyBuilings,
  enemyBuildings,
  missiles,
  emptyCells,
  enemyEnergyBuildings,
  enemyWalls,
  enemyTurrets,
  myEnergyProduction
}

function initBot(args) {
  key = args[0]
  workingDirectory = args[1]
}

/***
 * Returns an array with one less level of nesting
 * @param array
 * @returns {Array}
 */
function flatMap(array) {
  return array.reduce((acc, x) => acc.concat(x), [])
}
