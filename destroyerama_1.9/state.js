console.time("state generation")
const DEFENSE = 0
const ATTACK = 1
const ENERGY = 2
let stateFile = require("./state.json")
let helpers = require("./helpers.js")
let flatMap = helpers.flatMap
let unique = helpers.unique
let most = helpers.most
let least = helpers.least
let allHighest = helpers.allHighest
let myself = stateFile.players.find(p => p.playerType === "A")
let opponent = stateFile.players.find(p => p.playerType === "B")
let mapSize = {
  x: stateFile.gameDetails.mapWidth,
  y: stateFile.gameDetails.mapHeight
}
let buildingStats = []
buildingStats[0] = stateFile.gameDetails.buildingsStats.DEFENSE
buildingStats[1] = stateFile.gameDetails.buildingsStats.ATTACK
buildingStats[2] = stateFile.gameDetails.buildingsStats.ENERGY
let currentRound = stateFile.gameDetails.round
let gameMap = stateFile.gameMap
let cells = flatMap(gameMap) // all cells on the entire ma
let buildings = flatMap(cells.filter(cell => cell.buildings.length > 0).map(cell => cell.buildings))
instantiateCellProperties()
addImpactInXRounds()
let emptyCells = removeDestroyedBuildings()
addImpactInXRoundsForNewTurrets() // must run after removeDestroyedBuildings
let myBuildings = buildings.filter(building => building.playerType == "A")
let myEnergyBuilings = myBuildings.filter(building => building.buildingType == "ENERGY")
let myTurrets = myBuildings.filter(building => building.buildingType == "ATTACK")
let enemyBuildings = buildings.filter(building => building.playerType == "B")
// let emptyCells = cells.filter(cell => cell.buildings.length == 0 && cell.x <= mapSize.x / 2 - 1) // cells without buildings on them and on my half of the map
let enemyEnergyBuildings = enemyBuildings.filter(building => building.buildingType == "ENERGY")
let enemyWalls = enemyBuildings.filter(building => building.buildingType == "DEFENSE")
let enemyTurrets = enemyBuildings.filter(building => building.buildingType == "ATTACK")
let missiles = flatMap(cells.filter(cell => cell.missiles.length > 0).map(cell => cell.missiles))
let enemyMissiles = missiles.filter(missile => missile.playerType == "B")
let myMissiles = missiles.filter(missile => missile.playerType == "A")
let myEnergyProduction = myEnergyBuilings.length * buildingStats[ENERGY].energyGeneratedPerTurn
let validYCoords = getValidYCoords()
let validXCoords = getValidXCoords()
const defenceCapabilityRating = {
  ENERGY: 1,
  ATTACK: 2,
  DEFENSE: 5
}
const targetPriorityRating = {
  ENERGY: 1,
  ATTACK: 5,
  DEFENSE: 0
}
const attackRating = {
  ENERGY: 0,
  ATTACK: 2,
  DEFENSE: 1
}
const coverProvidedToRating = {
  ENERGY: 5,
  ATTACK: 2,
  DEFENSE: 0
}
const impactInXRoundsLookup = [0, 10, 9, 8, 7, 6, 5]

addEmptyCellsAdditionalStats()
addEnemyBuildingsAdditionalStats()
addTurretViability() // must run after addEmptyCellsAdditionalStats

// console.log(cellsWithEnemyMissiles)
// let moster = most(emptyCells, "energyViability")
// console.log(missiles)
// console.log(enemyMissiles)
// let impacts = cells.filter(cell => cell.impactInXRounds)
// console.log(impacts)

console.log(enemyBuildings)
let allMostViableTurretLocations = allHighest(emptyCells, "turretViability")
let mostViable = most(emptyCells, "turretViability")
console.log(enemyTurrets)
// console.log(allMost)
console.log(emptyCells)
console.log(buildings)

console.timeEnd("state generation")
console.log("end")
function initBot(args) {
  key = args[0]
  workingDirectory = args[1]
}

function getValidYCoords() {
  let maxYCoord = mapSize.y - 1
  let validYCoords = []
  for (let index = 0; index <= maxYCoord; index++) {
    validYCoords.push(index)
  }
  return validYCoords
}

function getValidXCoords() {
  let maxXCoord = mapSize.x / 2 - 1
  let validXCoords = []
  for (let index = 0; index <= maxXCoord; index++) {
    validXCoords.push(index)
  }
  return validXCoords
}

function getYCoordsAttackRating() {
  // SG_TODO: MAYBE could improve by checking walls that don't have
  // turrets behind them and lessening their rating or looking for wall & turret
  // combinations for higher rating?
  return enemyBuildings.reduce((obj, building) => {
    if (!obj[building.y]) {
      obj[building.y] = 0
    }
    obj[building.y] += attackRating[building.buildingType]
    return obj
  }, {})
}

function instantiateCellProperties() {
  cells.forEach(cell => {
    // cell.vulnerability = 0
    cell.impactInXRounds = 0 // will be set on all cells within missile range for next round
  })
}

function addImpactInXRounds() {
  addImpactInXRoundsBasedOnCurrentMissiles()
  addImpactInXRoundsBasedOnWeaponCooldown()
}

function addImpactInXRoundsBasedOnCurrentMissiles() {
  let missiles = flatMap(cells.filter(cell => cell.missiles.length > 0).map(cell => cell.missiles))
  let enemyMissiles = missiles.filter(missile => missile.playerType == "B")
  let myMissiles = missiles.filter(missile => missile.playerType == "A")
  enemyMissiles.forEach(missile => {
    addMissileLocation(missile)
  })
  myMissiles.forEach(missile => {
    addMissileLocation(missile, true)
  })
}

function addMissileLocation(missileOrTurret, isFriendly = false) {
  // turret also passed in instead of missile for weaponCooldown finishing
  if (!missileOrTurret.hasOwnProperty("speed")) {
    missileOrTurret.speed = missileOrTurret.weaponSpeed
  }
  let numberOfColumnsToCheck
  if (isFriendly) {
    numberOfColumnsToCheck = gameMap[0].length - 1 - missileOrTurret.x // -1 because valid x values will be lenght - 1
  } else {
    numberOfColumnsToCheck = missileOrTurret.x
  }
  for (let index = 1; index <= numberOfColumnsToCheck; index++) {
    let cellToCheck
    if (isFriendly) {
      cellToCheck = cells.find(cell => cell.y == missileOrTurret.y && cell.x == missileOrTurret.x + index)
    } else {
      cellToCheck = cells.find(cell => cell.y == missileOrTurret.y && cell.x == missileOrTurret.x - index)
    }
    if (cellToCheck.impactInXRounds > 0) {
      return // otherwise we calculate the next round of missiles (coming from the turret) and add it on top
    }
    if (!cellToCheck) {
      return
    }
    let roundsUntilImpact = Math.round(Math.abs(missileOrTurret.x - cellToCheck.x) / missileOrTurret.speed)
    cellToCheck.impactInXRounds += roundsUntilImpact
    if (cellToCheck.buildings.length) {
      break // stop checking if it hits something
    }
  }
}

function addImpactInXRoundsBasedOnWeaponCooldown() {
  let turrets = buildings.filter(building => building.buildingType == "ATTACK")
  turrets.forEach(turret => {
    // exclude turrets still under construction
    // see addImpactInXRoundsForNewTurrets
    if ([0, 1].includes(turret.weaponCooldownTimeLeft && turret.constructionTimeLeft == -1)) {
      addMissileLocation(turret, turret.playerType == "A") // use turret as missile calc
    }
  })
}

function addImpactInXRoundsForNewTurrets() {
  // have to do it seperate from WeaponCooldown ones so that it first removes destroyed ones. Otherwise it will fire missiles from destroyed turrets
  let newTurrets = buildings.filter(building => building.buildingType == "ATTACK" && building.constructionTimeLeft == 0)
  newTurrets.forEach(turret => {
    addMissileLocation(turret, turret.playerType == "A") // use turret as missile calc
  })
}

function removeDestroyedBuildings() {
  let emptyCells = cells.filter(cell => cell.buildings.length == 0 && cell.x <= mapSize.x / 2 - 1) // cells without buildings on them and on my half of the map
  let destroyed = buildings.filter(building => {
    let cell = cells.find(cell => cell.x == building.x && cell.y == building.y)
    if (cell.impactInXRounds == 1 && building.health - buildingStats[ATTACK].weaponDamage <= 0) {
      // emptyCells.push(cell) // can't push to emptyCells as game engen places player moves before destrying buildings
      return true
    }
    return false
  })
  buildings = buildings.filter(building => {
    let cell = cells.find(cell => cell.x == building.x && cell.y == building.y)
    return cell.impactInXRounds != 1 || building.health - buildingStats[ATTACK].weaponDamage > 0
  })
  // why does it think here 8x6 gets destroyed instead of 6x6
  // console.log(destroyed)
  // console.log("test")
  return emptyCells
}

function addEmptyCellsAdditionalStats() {
  let yCoordsBeingAttacked = getYCoordsAttackRating()
  emptyCells.forEach(emptyCell => {
    // emptyCell.vulnerability = yCoordsBeingAttacked[emptyCell.y] || 0
    // addVulnerabilityBasedOnImpactInXRounds(emptyCell)
    emptyCell.potentialCoverProvided = getPotentialCoverProvidedRating(emptyCell.y)
    // SG_TODO: change functions above to be same format as below
    addWallViability(emptyCell)
    addEnergyViability(emptyCell)
  })
}

// function addVulnerabilityBasedOnImpactInXRounds(emptyCell) {
//   // if (emptyCell.impactInXRounds > buildingStats[ATTACK].constructionTime) { // can maybe rather do something like this before it places the building,
//   // so you could check what type of building you're trying to build.
//   // minus 1 because constructiontime for turret is 2 rounds
//   emptyCell.vulnerability += impactInXRoundsLookup[emptyCell.impactInXRounds] // SG_TODO: fix what if  rounds is more than lookup
//   // }
// }

function getPotentialCoverProvidedRating(yCoord) {
  let buildingsBehind = myBuildings.filter(building => building.y == yCoord)
  let potentialCoverProvided = 0
  buildingsBehind.forEach(building => {
    potentialCoverProvided += coverProvidedToRating[building.buildingType]
  })
  return potentialCoverProvided
}

function addWallViability(emptyCell) {
  emptyCell.wallViability = 0
  // emptyCell.wallViability -= emptyCell.vulnerability
  emptyCell.wallViability += emptyCell.potentialCoverProvided
}

function addEnergyViability(emptyCell) {
  emptyCell.energyViability = 0
  // emptyCell.energyViability -= emptyCell.vulnerability
  emptyCell.energyViability -= getEnergyOnSameRowCount(emptyCell.y) // spread out is better
  emptyCell.energyViability -= emptyCell.x // further back is better
}

function addTurretViability() {
  emptyCells.forEach(emptyCell => {
    emptyCell.turretViability = 0
    // emptyCell.turretViability -= emptyCell.vulnerability
    addTurretViabilityByEnemyEnergyBuildingsOnSameRow(emptyCell)
    addTurretViabilityByInitialTargetType(emptyCell)
    // emptyCell.turretViability +=
    // emptyCell.turretViability -= emptyCell.x // further back is better
  })
}

function addTurretViabilityByEnemyEnergyBuildingsOnSameRow(emptyCell) {
  let enemyEnergyBuildingsOnSameRow = enemyEnergyBuildings.filter(building => building.y == emptyCell.y)
  enemyEnergyBuildingsOnSameRow.forEach(building => {
    emptyCell.turretViability++
  })
}

function addTurretViabilityByInitialTargetType(emptyCell) {
  let enemyBuildingsOnSameRow = enemyBuildings.filter(building => building.y == emptyCell.y)
  if (enemyBuildingsOnSameRow.length) {
    let intialTarget = least(enemyBuildingsOnSameRow, "x")
    emptyCell.turretViability += targetPriorityRating[intialTarget.buildingType]
  }
}

function getEnergyOnSameRowCount(yCoord) {
  return myEnergyBuilings.filter(building => building.y == yCoord).length
}

function addEnemyBuildingsAdditionalStats() {
  let groupedByYCoord = enemyBuildings.reduce((obj, building) => {
    if (!obj[building.y]) {
      obj[building.y] = []
    }
    obj[building.y].push(building)
    return obj
  }, {})
  groupedByYCoord = Object.values(groupedByYCoord)
  groupedByYCoord.forEach(y_group => {
    y_group.forEach(building => {
      // addTargetPriority(y_group, building)
    })
  })
}

// function addTargetPriority(y_group, building) {
//   // base + buildingPriority + coverProvided - currentTurretsAlreadyAttacking
//   if (!building.hasOwnProperty("targetPriority")) {
//     building.targetPriority = 0
//   }
//   console.log(enemyBuildings)

//   addPriorityBasedOnBuildingType(building)
//   addPriorityBasedOnEnergyCoverProvided(building)
//   addPriorityBasedOnAdditionalTargetsBehindInitialTarget(building)
//   subtractPriorityBasedOnCurrentTurretsAlreadyAttacking(building)
//   if (building.buildingType == "DEFENSE" || building.buildingType == "ATTACK") {
//     addPriorityBasedOnConstructionTimeLeft(building)
//   }
// }

// function addPriorityBasedOnBuildingType(building) {
//   building.targetPriority += targetPriorityRating[building.buildingType]
// }

// function addPriorityBasedOnConstructionTimeLeft(building) {
//   building.targetPriority += building.constructionTimeLeft
//   if (building.buildingType == "ATTACK") {
//     building.targetPriority += 1
//   }
// }

// function addPriorityBasedOnEnergyCoverProvided(building) {
//   let energyBuildingsCoverProvided = getEnergyBuildinsgCoverProvidedTo(building.y)
//   building.targetPriority += energyBuildingsCoverProvided
// }

// function addPriorityBasedOnAdditionalTargetsBehindInitialTarget(building) {
//   let additionalTargets = getAdditionalBuildingsBehindInitialTarget(building.x, building.y)
//   additionalTargets.forEach(additionalTarget => addPriorityBasedOnBuildingType(building))
// }

// function subtractPriorityBasedOnCurrentTurretsAlreadyAttacking(building) {
//   building.targetPriority -= getCurrentAttackingTurretsCount(building.y)
// }

// function getCurrentAttackingTurretsCount(yCoord) {
//   return myTurrets.filter(turret => turret.y == yCoord).length
// }

// function getEnergyBuildinsgCoverProvidedTo(yCoord) {
//   return myEnergyBuilings.filter(energy => energy.y == yCoord).length
// }

// function getAdditionalBuildingsBehindInitialTarget(xCoord, yCoord) {
//   return enemyBuildings.filter(building => building.y == yCoord && building.x != xCoord)
// }

module.exports = {
  ATTACK,
  DEFENSE,
  ENERGY,
  myself,
  opponent,
  mapSize,
  buildingStats,
  cells,
  buildings,
  myBuildings,
  myEnergyBuilings,
  enemyBuildings,
  emptyCells,
  enemyEnergyBuildings,
  enemyWalls,
  enemyTurrets,
  myEnergyProduction,
  validXCoords,
  validYCoords,
  currentRound
}
