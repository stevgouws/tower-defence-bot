// "use strict"
let fs = require("fs")
let state = require("./state.js")
let currentRound = state.currentRound
let helpers = require("./helpers")
let unique = helpers.unique
let random = helpers.random
let allHighest = helpers.allHighest
let allLowest = helpers.allLowest
let least = helpers.least
let most = helpers.most
let commandFileName = "command.txt"
let myself = state.myself
let opponent = state.opponent
let mapSize = state.mapSize
let buildingStats = state.buildingStats
let cells = state.cells
let buildings = state.buildings
let myBuildings = state.myBuildings
let myEnergyBuildings = state.myEnergyBuilings
let enemyBuildings = state.enemyBuildings
// let missiles = state.missiles
let emptyCells = state.emptyCells
let enemyEnergyBuildings = state.enemyEnergyBuildings
let enemyWalls = state.enemyWalls
let enemyTurrets = state.enemyTurrets
let myEnergyProduction = state.myEnergyProduction
let validXCoords = state.validXCoords
let validYCoords = state.validYCoords
const DEFENSE = state.DEFENSE
const ATTACK = state.ATTACK
const ENERGY = state.ENERGY
runStrategy()

function runStrategy() {
  if (!haveOptimalEnergy()) {
    buildEnergy()
  } else {
    attack()
  }
}

function haveOptimalEnergy() {
  // basically build energy if the next round would otherwise drop you to having less energy than the cost to build more energy
  return myself.energy + myEnergyProduction - buildingStats[ATTACK].price > buildingStats[ENERGY].price
}

function buildEnergy() {
  let allMostViable = allHighest(emptyCells, "energyViability")
  allMostViable = checkForImminentAttack(allMostViable)
  let { x, y } = most(allMostViable, "energyViability")
  buildCommand(x, y, ENERGY)
}

function attack() {
  let allMostViable = allHighest(emptyCells, "turretViability")
  allMostViable = checkForImminentAttack(allMostViable)
  let { x, y } = most(allMostViable, "x")
  buildCommand(x, y, ATTACK)
}

function checkForImminentAttack(allMostViable) {
  let allMostViableNotAboutToBeHit = allMostViable.filter(
    emptyCell => buildingStats[ENERGY].constructionTime > emptyCell.impactInXRounds
  )
  if (!allMostViableNotAboutToBeHit.length) {
    return allMostViable
  }
  return allMostViableNotAboutToBeHit
}

function buildRandom() {
  if (emptyCells.length == 0) {
    doNothing()
    return
  }
  let randomCell = getRandomFromArray(emptyCells)

  let command = { x: "", y: "", bt: "" }
  command.x = randomCell.x
  command.y = randomCell.y
  command.bt = getRandomInteger(2)
  buildCommand(command.x, command.y, command.bt)
}

function buildCommand(x, y, buildingType) {
  if (!enoughEnergy(buildingType)) {
    console.error("XXX: Nothing built")
    return doNothing()
  }
  if (cellIsEmpty(x, y)) {
    // SG_TODO: fix reading of empty cells
    console.log("writing to file")
    console.log(`${x}, ${y}, ${buildingType}`)

    writeToFile(commandFileName, `${x},${y},${buildingType}`)
  } else {
    if (y == 7) {
      x--
      y = -1
    }
    buildCommand(x, y + 1, buildingType)
  }
}

function cellIsEmpty(x, y) {
  return Boolean(emptyCells.find(cell => cell.x == x && cell.y == y))
}

function enoughEnergy(buildingType) {
  console.log(buildingStats[buildingType].price)
  console.log(myself.energy)

  return buildingStats[buildingType].price <= myself.energy
}

function doNothing() {
  writeToFile(commandFileName, ``)
}

function writeToFile(fileName, payload) {
  fs.writeFile("./" + fileName, payload, err => {
    console.log("wrote")
    if (err) {
      return console.log(err)
    }
    // console.log(payload);
  })
}

/***
 * Returns a random integer between 0(inclusive) and max(inclusive)
 * @param max
 * @returns {number}
 */
function getRandomInteger(max) {
  return Math.round(Math.random() * max)
}

/**
 * Returns an array that is filled with integers from 0(inclusive) to count(inclusive)
 * @param count
 * @returns {number[]}
 */
function getArrayRange(count) {
  return Array.from({ length: count }, (v, i) => i)
}

/**
 * Return a random element from a given array
 * @param array
 * @returns {*}
 */
function getRandomFromArray(array) {
  return array[Math.floor(Math.random() * array.length)]
}
