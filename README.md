This is my entry for the [2018 Entellect bot war challenge](https://challenge.entelect.co.za/rules) where I placed #11 out of 98.

The final version I submitted to the competition can be found [here](https://gitlab.com/stevgouws/tower-defence-bot/tree/master/destroyerama_1.16).
